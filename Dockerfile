FROM openjdk:8-jre-alpine
ENV TZ=Asia/Bangkok
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
COPY ./target/SpringBootsCICD-0.0.1-SNAPSHOT.jar /data/
WORKDIR /data/
CMD ["java", "-jar", "SpringBootsCICD-0.0.1-SNAPSHOT.jar"]