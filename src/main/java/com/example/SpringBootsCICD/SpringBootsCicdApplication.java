package com.example.SpringBootsCICD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootsCicdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootsCicdApplication.class, args);
	}

}
